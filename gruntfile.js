module.exports = function(grunt) {
  var config = {
    pkg: require('./package.json'),
    env: process.env,
    src_js_lib: []
      //src_mocks: 'src/bower_components/angular-mocks/angular-mocks.js',
      //app_js_files: ['src/app/app.js', 'src/app/**/*module.js', 'src/app/**/*config.js', 'src/app/**/*.js'],
      //test_js_files: ['src/app/**/*spec.js']
  };
  var configLoader = function(path) {
      var glob = require('glob');
      var object = {};
      var key;
      glob.sync('*', {
        cwd: path
      }).forEach(function(option) {
        key = option.replace(/\.js$/, '');
        object[key] = require(path + option);
      });
      return object;
    }
    // Load all of the options from the options folder
  grunt.util._.extend(config, configLoader('./grunt_tasks/'));
  grunt.initConfig(config);
  // grunt.loadNpmTasks('grunt-contrib-jshint');
  // grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-connect');
  // grunt.loadNpmTasks('grunt-contrib-concat');
  // grunt.loadNpmTasks('grunt-contrib-less');
  // grunt.registerTask('default', ['less', 'jshint', 'connect']);
};